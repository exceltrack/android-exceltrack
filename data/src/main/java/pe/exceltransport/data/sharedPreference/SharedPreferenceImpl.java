package pe.exceltransport.data.sharedPreference;


import com.google.gson.Gson;
import com.pddstudio.preferences.encrypted.EncryptedPreferences;

import javax.inject.Inject;

import io.reactivex.Observable;
import pe.exceltransport.data.BuildConfig;
import pe.exceltransport.data.entity.SessionEntity;
import pe.exceltransport.data.exception.DefaultException;

public class SharedPreferenceImpl implements SharedPreference {

    public static final String KEY = BuildConfig.PREFERENCE_KEY;

    private static final String KEY_USERNAME = "key_username";
    private static final String KEY_SESSION = "key_session";

    private final EncryptedPreferences encryptedPreferences;

    @Inject
    public SharedPreferenceImpl(EncryptedPreferences encryptedPreferences) {
        this.encryptedPreferences = encryptedPreferences;
    }

    @Override
    public Observable<Void> saveSession(String session) {
        return Observable.create(emitter -> {
            encryptedPreferences.edit()
                    .putString(encryptedPreferences.getUtils().encryptStringValue(KEY_SESSION), session)
                    .apply();
            emitter.onComplete();
        });
    }

    @Override
    public Observable<Void> saveUsername(String username) {
        return Observable.create(emitter -> {
            encryptedPreferences.edit()
                    .putString(encryptedPreferences.getUtils().encryptStringValue(KEY_USERNAME), username)
                    .apply();
            emitter.onComplete();
        });
    }

    @Override
    public Observable<String> getUsernameSaved() {
        return Observable.create(emitter -> {
            String email = encryptedPreferences.getString(encryptedPreferences.getUtils().encryptStringValue(KEY_USERNAME), "");
            emitter.onNext(email);
            emitter.onComplete();
        });
    }

    @Override
    public Observable<SessionEntity> getSessionSaved() {
        return Observable.create(emitter -> {
            String session = encryptedPreferences.getString(encryptedPreferences.getUtils().encryptStringValue(KEY_SESSION), "");
            if(!session.isEmpty() ){
                emitter.onNext(new Gson().fromJson(session, SessionEntity.class));
                emitter.onComplete();
            }else{
                emitter.onError(new DefaultException(DefaultException.Codes.NO_SESSION.getCode()));
            }
        });
    }

}