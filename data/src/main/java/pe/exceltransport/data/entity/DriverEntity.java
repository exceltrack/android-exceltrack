package pe.exceltransport.data.entity;

import com.google.gson.annotations.SerializedName;

public class DriverEntity {

    @SerializedName("id")
    private long id;

    @SerializedName("user")
    private UserEntity user;

    @SerializedName("tractor")
    private VehicleEntity tractor;

    @SerializedName("trailer")
    private VehicleEntity trailer;


    public long getId() {
        return id;
    }

    public UserEntity getUser() {
        return user;
    }

    public VehicleEntity getTractor() {
        return tractor;
    }

    public VehicleEntity getTrailer() {
        return trailer;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void setTractor(VehicleEntity tractor) {
        this.tractor = tractor;
    }

    public void setTrailer(VehicleEntity trailer) {
        this.trailer = trailer;
    }
}
