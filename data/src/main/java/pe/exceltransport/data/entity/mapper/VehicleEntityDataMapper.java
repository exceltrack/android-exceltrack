package pe.exceltransport.data.entity.mapper;

import pe.exceltransport.data.entity.VehicleEntity;
import pe.exceltransport.domain.Vehicle;

public class VehicleEntityDataMapper {

    private VehicleEntityDataMapper() {
        //empty constructor
    }

    static Vehicle transform(VehicleEntity entity) {
        if (entity == null) {
            return null;
        }
        Vehicle vehicle = new Vehicle();
        vehicle.setId(entity.getId());
        vehicle.setPlate(entity.getPlate());
        vehicle.setBrand(entity.getBrand());
        vehicle.setModel(entity.getModel());
        vehicle.setType(transform(entity.getType()));
        vehicle.setTankCapacity(entity.getTankCapacity());
        return vehicle;
    }

    public static VehicleEntity transform(Vehicle vehicle) {
        if (vehicle == null) {
            return null;
        }
        VehicleEntity entity = new VehicleEntity();
        entity.setId(vehicle.getId());
        entity.setPlate(vehicle.getPlate());
        entity.setBrand(vehicle.getBrand());
        entity.setModel(vehicle.getModel());
        entity.setType(vehicle.getType().ordinal());
        entity.setTankCapacity(vehicle.getTankCapacity());
        return entity;

    }

    private static Vehicle.Type transform(int type) {
        switch (type) {
            case 0:
                return Vehicle.Type.TRACTOR;
            case 1:
                return Vehicle.Type.TRAILER_20;
            case 2:
                return Vehicle.Type.TRAILER_40;
            default:
                return Vehicle.Type.TRAILER_REFRIGERATED;
        }
    }

}
