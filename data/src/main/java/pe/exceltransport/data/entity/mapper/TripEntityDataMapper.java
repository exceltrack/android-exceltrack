package pe.exceltransport.data.entity.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.exceltransport.data.entity.TripEntity;
import pe.exceltransport.domain.Trip;

public class TripEntityDataMapper {

    private TripEntityDataMapper(){
        //empty constructor
    }

    static Trip transform(TripEntity entity){
        if(entity == null){
            return null;
        }
        Trip trip = new Trip();
        trip.setId(entity.getId());
        trip.setReferenceDocument(entity.getReferenceDocument());
        trip.setLoad(transformLoad(entity.getLoad()));
        trip.setInitialFuelTank(entity.getInitialFuelTank());
        trip.setFinalFuelTank(entity.getFinalFuelTank());
        trip.setStart(LocationEntityDataMapper.transform(entity.getStart()));
        trip.setFinish(LocationEntityDataMapper.transform(entity.getFinish()));
        trip.setStatus(transformStatus(entity.getStatus()));
        trip.setCustomer(CustomerEntityDataMapper.transform(entity.getCustomer()));
        trip.setStartDate(entity.getStartDate());
        trip.setContactName(entity.getContactName());
        trip.setContectPhone(entity.getContactPhone());
        return trip;
    }

    static TripEntity transform(Trip trip){
        if (trip == null) {
            return null;
        }
        TripEntity entity = new TripEntity();
        entity.setId(trip.getId());
        entity.setReferenceDocument(trip.getReferenceDocument());
        entity.setLoad(trip.getLoad().ordinal());
        entity.setInitialFuelTank(trip.getInitialFuelTank());
        entity.setFinalFuelTank(trip.getFinalFuelTank());
        entity.setStart(LocationEntityDataMapper.transform(trip.getStart()));
        entity.setFinish(LocationEntityDataMapper.transform(trip.getFinish()));
        entity.setStatus(trip.getStatus().ordinal());
        entity.setCustomer(CustomerEntityDataMapper.transform(trip.getCustomer()));
        return entity;
    }

    private static Trip.Status transformStatus(int status){
        if(status == 0){
            return Trip.Status.PENDING;
        }else if(status == 1){
            return Trip.Status.ACTIVE;
        }else{
            return Trip.Status.COMPLETED;
        }
    }

    private static Trip.Load transformLoad(int load){
        switch (load) {
            case 0:
                return Trip.Load.REFRIGERATED_CONTAINER;
            case 1:
                return Trip.Load.DRY_LOOSE;
            case 2:
                return Trip.Load.DRY_CONTAINER_20;
            default:
                return Trip.Load.DRY_CONTAINER_40;
        }
    }

    public static List<Trip> transform(List<TripEntity> entities){
        ArrayList<Trip> list = new ArrayList<>();
        if(entities == null){
            return list;
        }
        for (TripEntity entity : entities){
            list.add(transform(entity));
        }
        return list;
    }

}
