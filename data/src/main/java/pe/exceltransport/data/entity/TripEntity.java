package pe.exceltransport.data.entity;



import com.google.gson.annotations.SerializedName;

public class TripEntity {

    @SerializedName("id")
    private long id;

    @SerializedName("reference_document")
    private String referenceDocument;

    @SerializedName("load")
    private int load;

    @SerializedName("initial_fuel_tank")
    private double initialFuelTank;

    @SerializedName("final_fuel_tank")
    private double finalFuelTank;

    @SerializedName("start")
    private LocationEntity start;

    @SerializedName("finish")
    private LocationEntity finish;

    @SerializedName("status")
    private int status;

    @SerializedName("customer")
    private CustomerEntity customer;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("customer_contact_name")
    private String contactName;

    @SerializedName("customer_contact_number")
    private String contactPhone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public double getInitialFuelTank() {
        return initialFuelTank;
    }

    public void setInitialFuelTank(double initialFuelTank) {
        this.initialFuelTank = initialFuelTank;
    }

    public double getFinalFuelTank() {
        return finalFuelTank;
    }

    public void setFinalFuelTank(double finalFuelTank) {
        this.finalFuelTank = finalFuelTank;
    }

    public LocationEntity getStart() {
        return start;
    }

    public void setStart(LocationEntity start) {
        this.start = start;
    }

    public LocationEntity getFinish() {
        return finish;
    }

    public void setFinish(LocationEntity finish) {
        this.finish = finish;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }
}
