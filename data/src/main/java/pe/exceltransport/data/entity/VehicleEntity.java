package pe.exceltransport.data.entity;

import com.google.gson.annotations.SerializedName;

public class VehicleEntity {

    @SerializedName("id")
    private long id;

    @SerializedName("plate")
    private String plate;

    @SerializedName("type")
    private int type;

    @SerializedName("brand")
    private String brand;

    @SerializedName("model")
    private String model;

    @SerializedName("tank_capacity")
    private double tankCapacity;

    public long getId() {
        return id;
    }

    public String getPlate() {
        return plate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getTankCapacity() {
        return tankCapacity;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setTankCapacity(double tankCapacity) {
        this.tankCapacity = tankCapacity;
    }
}
