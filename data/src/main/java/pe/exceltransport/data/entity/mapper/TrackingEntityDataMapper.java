package pe.exceltransport.data.entity.mapper;


import pe.exceltransport.data.entity.TrackingEntity;
import pe.exceltransport.domain.Tracking;

public class TrackingEntityDataMapper {

    private TrackingEntityDataMapper(){
        //empty constructor
    }

    public static Tracking transform(TrackingEntity entity){
        if(entity == null){
            return null;
        }
        Tracking tracking = new Tracking();
        tracking.setTrackingId(entity.getId());
        tracking.setStatus(transform(entity.getStatus()));
        tracking.setEvents(EventEntityDataMapper.transform(entity.getEvents()));
        return tracking;
    }

    private static Tracking.Status transform(int status){
        switch (status) {
            case 0:
                return Tracking.Status.ASSIGNED;
            case 1:
                return Tracking.Status.STARTED;
            case 2:
                return Tracking.Status.CHECK_IN;
            case 3:
                return Tracking.Status.LOAD;
            case 4:
                return Tracking.Status.DEPARTURE;
            case 5:
                return Tracking.Status.ARRIVAL;
            case 6:
                return Tracking.Status.UNLOADING;
            case 7:
                return Tracking.Status.RETURN;
            default:
                return Tracking.Status.COMPLETED;
        }
    }

}
