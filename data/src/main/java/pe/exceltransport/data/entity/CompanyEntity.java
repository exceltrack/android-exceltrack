package pe.exceltransport.data.entity;

import com.google.gson.annotations.SerializedName;

public class CompanyEntity {

    @SerializedName("id")
    private long id;

    @SerializedName("business_name")
    private String businessName;

    @SerializedName("trade_name")
    private String tradeName;

    @SerializedName("ruc")
    private String ruc;

    @SerializedName("registered_address")
    private LocationEntity registeredAddress;

    public long getId() {
        return id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getTradeName() {
        return tradeName;
    }

    public String getRuc() {
        return ruc;
    }

    public LocationEntity getRegisteredAddress() {
        return registeredAddress;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setRegisteredAddress(LocationEntity registeredAddress) {
        this.registeredAddress = registeredAddress;
    }
}
