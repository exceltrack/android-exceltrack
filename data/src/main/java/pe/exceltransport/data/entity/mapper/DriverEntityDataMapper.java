package pe.exceltransport.data.entity.mapper;

import pe.exceltransport.data.entity.DriverEntity;
import pe.exceltransport.data.entity.UserEntity;
import pe.exceltransport.domain.Driver;

public class DriverEntityDataMapper {

    private DriverEntityDataMapper() {
        //empty constructor
    }

    public static Driver transform(DriverEntity entity) {
        if (entity == null) {
            return null;
        }
        Driver driver = new Driver();
        driver.setId(entity.getId());
        driver.setUser(UserEntityDataMapper.transform(entity.getUser()));
        driver.setTractor(VehicleEntityDataMapper.transform(entity.getTractor()));
        driver.setTrailer(VehicleEntityDataMapper.transform(entity.getTrailer()));
        return driver;
    }

    public static DriverEntity transform(Driver driver) {
        if (driver == null) {
            return null;
        }
        DriverEntity entity = new DriverEntity();
        entity.setId(driver.getId());
        entity.setUser(UserEntityDataMapper.transform(driver.getUser()));
        entity.setTractor(VehicleEntityDataMapper.transform(driver.getTractor()));
        entity.setTrailer(VehicleEntityDataMapper.transform(driver.getTrailer()));
        return entity;
    }
}
