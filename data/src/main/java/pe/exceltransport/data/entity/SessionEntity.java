package pe.exceltransport.data.entity;

import com.google.gson.annotations.SerializedName;

public class SessionEntity {

    @SerializedName("access_token")
    private String token;

    @SerializedName("driver")
    private DriverEntity driver;

    @SerializedName("trip")
    private TripEntity trip;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    public TripEntity getTrip() {
        return trip;
    }

    public void setTrip(TripEntity trip) {
        this.trip = trip;
    }
}
