package pe.exceltransport.data.network;

import pe.exceltransport.data.network.body.EventBody;
import pe.exceltransport.data.network.body.SignInBody;
import pe.exceltransport.data.network.response.BodyResponse;
import pe.exceltransport.data.network.response.DriverResponse;
import pe.exceltransport.data.network.response.SignInResponse;
import pe.exceltransport.data.network.response.TrackingResponse;
import pe.exceltransport.data.network.response.TripsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {

    @Headers("Content-Type: application/json")
    @POST("users/auth")
    Call<BodyResponse<SignInResponse>> signIn(@Body SignInBody bodyLogin);

    @Headers("Content-Type: application/json")
    @GET("drivers/{driverId}/trips")
    Call<BodyResponse<TripsResponse>> getTrips(@Header("Authorization") String auth, @Path("driverId") long customerId, @Query("status") int status);


    @Headers("Content-Type: application/json")
    @GET("drivers/{driverId}")
    Call<BodyResponse<DriverResponse>> getDriver(@Header("Authorization") String auth, @Path("driverId") long driverId);


    @Headers("Content-Type: application/json")
    @GET("trips/{tripId}/trackings")
    Call<BodyResponse<TrackingResponse>> getTracking(@Header("Authorization") String auth, @Path("tripId") long tripId);

    @Headers("Content-Type: application/json")
    @POST("trackings/{trackingId}/events")
    Call<BodyResponse<TrackingResponse>> addEvent(@Header("Authorization") String auth, @Path("trackingId") long trackingId, @Body EventBody body);

}
