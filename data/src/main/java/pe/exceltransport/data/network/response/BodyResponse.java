package pe.exceltransport.data.network.response;

import com.google.gson.annotations.SerializedName;

public class BodyResponse<T> {

    @SerializedName("data")
    private T data;

    @SerializedName("error")
    private ErrorResponse error;

    public T getData() {
        return data;
    }

    public ErrorResponse getError() {
        return error;
    }
}
