package pe.exceltransport.data.network;

import android.support.annotation.NonNull;

import io.reactivex.ObservableEmitter;
import pe.exceltransport.data.exception.DefaultException;
import pe.exceltransport.data.network.response.BodyResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefaultCallback<T> implements Callback<T> {

    private final ObservableEmitter emitter;

    DefaultCallback(ObservableEmitter emitter) {
        this.emitter = emitter;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (!response.isSuccessful()) {
            emitter.onError(new DefaultException(DefaultException.Codes.DEFAULT_ERROR.getCode()));
        } else {
            BodyResponse body = ((BodyResponse) response.body());
            if (body != null && body.getError() != null) {
                emitter.onError(new DefaultException(body.getError().getMessage(), body.getError().getCode()));
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        emitter.onError(new DefaultException(DefaultException.Codes.DEFAULT_ERROR.getCode()));
    }
}
