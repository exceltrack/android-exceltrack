package pe.exceltransport.data.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import pe.exceltransport.data.entity.DriverEntity;
import pe.exceltransport.data.entity.SessionEntity;
import pe.exceltransport.data.entity.TrackingEntity;
import pe.exceltransport.data.entity.TripEntity;
import pe.exceltransport.data.exception.DefaultException;
import pe.exceltransport.data.network.body.EventBody;
import pe.exceltransport.data.network.body.SignInBody;
import pe.exceltransport.data.network.response.BodyResponse;
import pe.exceltransport.data.network.response.DriverResponse;
import pe.exceltransport.data.network.response.SignInResponse;
import pe.exceltransport.data.network.response.TrackingResponse;
import pe.exceltransport.data.network.response.TripsResponse;
import retrofit2.Call;
import retrofit2.Response;

public class RestApiImpl implements RestApi {

    private final Context context;
    private final RestService restService;

    @Inject
    public RestApiImpl(Context context, RestService restService) {
        this.context = context;
        this.restService = restService;
    }

    @Override
    public Observable<SessionEntity> signIn(SignInBody body) {
        return Observable.create(emitter -> {
            if (isThereNetworkConnection(emitter)) {
                restService.signIn(body).enqueue(new DefaultCallback<BodyResponse<SignInResponse>>(emitter) {
                    @Override
                    public void onResponse(@NonNull Call<BodyResponse<SignInResponse>> call, @NonNull Response<BodyResponse<SignInResponse>> response) {
                        super.onResponse(call, response);
                        BodyResponse<SignInResponse> bodyResponse = response.body();
                        if (bodyResponse != null && bodyResponse.getData() != null) {
                            emitter.onNext(bodyResponse.getData().getSessionEntity());
                            emitter.onComplete();
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<DriverEntity> getDriver(String token, long driverId) {
        return Observable.create(emitter -> {
            if (isThereNetworkConnection(emitter)) {
                restService.getDriver(token, driverId).enqueue(new DefaultCallback<BodyResponse<DriverResponse>>(emitter) {
                    @Override
                    public void onResponse(@NonNull Call<BodyResponse<DriverResponse>> call, @NonNull Response<BodyResponse<DriverResponse>> response) {
                        super.onResponse(call, response);
                        BodyResponse<DriverResponse> bodyResponse = response.body();
                        if (bodyResponse != null && bodyResponse.getData() != null) {
                            emitter.onNext(bodyResponse.getData().getDriver());
                            emitter.onComplete();
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<List<TripEntity>> getTrips(String token, long driverId, int status) {
        return Observable.create(emitter -> {
            if (isThereNetworkConnection(emitter)) {
                restService.getTrips(token, driverId, status).enqueue(new DefaultCallback<BodyResponse<TripsResponse>>(emitter) {
                    @Override
                    public void onResponse(@NonNull Call<BodyResponse<TripsResponse>> call, @NonNull Response<BodyResponse<TripsResponse>> response) {
                        super.onResponse(call, response);
                        BodyResponse<TripsResponse> bodyResponse = response.body();
                        if (bodyResponse != null && bodyResponse.getData() != null) {
                            emitter.onNext(bodyResponse.getData().getTrips());
                            emitter.onComplete();
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<TrackingEntity> getTracking(String token, long tripId) {
        return Observable.create(emitter -> {
            if (isThereNetworkConnection(emitter)) {
                restService.getTracking(token, tripId).enqueue(new DefaultCallback<BodyResponse<TrackingResponse>>(emitter) {
                    @Override
                    public void onResponse(@NonNull Call<BodyResponse<TrackingResponse>> call, @NonNull Response<BodyResponse<TrackingResponse>> response) {
                        super.onResponse(call, response);
                        BodyResponse<TrackingResponse> bodyResponse = response.body();
                        if (bodyResponse != null && bodyResponse.getData() != null) {
                            emitter.onNext(bodyResponse.getData().getTrackin());
                            emitter.onComplete();
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<TrackingEntity> addEvent(String token, long trackingId, EventBody body) {
        return Observable.create(emitter -> {
            if (isThereNetworkConnection(emitter)) {
                restService.addEvent(token, trackingId, body).enqueue(new DefaultCallback<BodyResponse<TrackingResponse>>(emitter) {
                    @Override
                    public void onResponse(@NonNull Call<BodyResponse<TrackingResponse>> call, @NonNull Response<BodyResponse<TrackingResponse>> response) {
                        super.onResponse(call, response);
                        BodyResponse<TrackingResponse> bodyResponse = response.body();
                        if (bodyResponse != null && bodyResponse.getData() != null) {
                            emitter.onNext(bodyResponse.getData().getTrackin());
                            emitter.onComplete();
                        }
                    }
                });
            }
        });
    }


    private boolean isThereNetworkConnection(Emitter emitter) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) return true;
        emitter.onError(new DefaultException(DefaultException.Codes.NO_INTERNET.getCode()));
        return false;
    }
}
