package pe.exceltransport.data.network.body;

public class SignInBody {

    private String username;
    private String password;
    private int role;

    public void setEmail(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
