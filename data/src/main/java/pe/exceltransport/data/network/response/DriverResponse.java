package pe.exceltransport.data.network.response;

import com.google.gson.annotations.SerializedName;

import pe.exceltransport.data.entity.DriverEntity;

public class DriverResponse {

    @SerializedName("driver")
    private DriverEntity driver;

    public DriverEntity getDriver() {
        return driver;
    }
}
