package pe.exceltransport.data.network;

import java.util.List;

import io.reactivex.Observable;
import pe.exceltransport.data.entity.DriverEntity;
import pe.exceltransport.data.entity.SessionEntity;
import pe.exceltransport.data.entity.TrackingEntity;
import pe.exceltransport.data.entity.TripEntity;
import pe.exceltransport.data.network.body.EventBody;
import pe.exceltransport.data.network.body.SignInBody;

public interface RestApi {

    //user
    Observable<SessionEntity> signIn(SignInBody body);

    //driver
    Observable<DriverEntity> getDriver(String token, long driverId);

    //trip
    Observable<List<TripEntity>> getTrips(String token, long userId, int status);


    //tracking
    Observable<TrackingEntity> getTracking(String token, long tripId);

    Observable<TrackingEntity> addEvent(String token, long trackingId, EventBody body);


}
