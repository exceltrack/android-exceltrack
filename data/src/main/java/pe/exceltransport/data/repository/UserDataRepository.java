package pe.exceltransport.data.repository;

import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import pe.exceltransport.data.entity.DriverEntity;
import pe.exceltransport.data.entity.mapper.DriverEntityDataMapper;
import pe.exceltransport.data.entity.mapper.SessionEntityDataMapper;
import pe.exceltransport.data.network.RestApi;
import pe.exceltransport.data.network.body.SignInBody;
import pe.exceltransport.data.sharedPreference.SharedPreference;
import pe.exceltransport.domain.Driver;
import pe.exceltransport.domain.Session;
import pe.exceltransport.domain.repository.UserRepository;

public class UserDataRepository implements UserRepository {

    private final RestApi restApi;
    private final SharedPreference sharedPreference;

    @Inject
    UserDataRepository(RestApi restApi, SharedPreference sharedPreference) {
        this.restApi = restApi;
        this.sharedPreference = sharedPreference;
    }

    @Override
    public Observable<Session> signIn(String email, String password) {
        SignInBody body = new SignInBody();
        body.setEmail(email);
        body.setPassword(password);
        body.setRole(1);
        return restApi.signIn(body).map(SessionEntityDataMapper::transform);
    }

    @Override
    public Observable<Driver> getDriver(String token, long driverId) {
        return restApi.getDriver(token,driverId).map(DriverEntityDataMapper::transform);
    }

    @Override
    public Observable<Void> saveSession(Session session) {
        return sharedPreference.saveSession(new Gson().toJson(SessionEntityDataMapper.transform(session)));
    }

    @Override
    public Observable<Void> saveUsername(String email) {
        return sharedPreference.saveUsername(email);
    }

    @Override
    public Observable<String> getUsernameSaved() {
        return sharedPreference.getUsernameSaved();
    }

    @Override
    public Observable<Session> getSessionSaved() {
        return sharedPreference.getSessionSaved().map(SessionEntityDataMapper::transform);
    }

    @Override
    public Observable<Void> deleteSessionSaved() {
        return sharedPreference.saveSession("");
    }

}
