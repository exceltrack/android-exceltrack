package pe.exceltransport.domain.interactor;

import javax.inject.Inject;

import io.reactivex.Observable;
import pe.exceltransport.domain.Driver;
import pe.exceltransport.domain.executor.PostExecutionThread;
import pe.exceltransport.domain.executor.ThreadExecutor;
import pe.exceltransport.domain.repository.UserRepository;

public class GetDriver extends UseCase<Driver, GetDriver.Params> {

    private final UserRepository repository;

    @Inject
    public GetDriver(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    Observable<Driver> buildUseCaseObservable(Params params) {
        return repository.getDriver(params.token,params.driverId);
    }

    public static final class Params {

        private final String token;

        private final long driverId;

        Params(String token, long driverId) {
            this.token = token;
            this.driverId = driverId;
        }

        public static Params buildParams(String token, long driverId) {
            return new Params(token, driverId);
        }
    }
}
