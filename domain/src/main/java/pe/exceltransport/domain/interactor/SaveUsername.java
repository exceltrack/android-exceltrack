package pe.exceltransport.domain.interactor;

import javax.inject.Inject;

import io.reactivex.Observable;
import pe.exceltransport.domain.executor.PostExecutionThread;
import pe.exceltransport.domain.executor.ThreadExecutor;
import pe.exceltransport.domain.repository.UserRepository;

public class SaveUsername extends UseCase<Void,SaveUsername.Params>{

    private final UserRepository repository;

    @Inject
    public SaveUsername(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return repository.saveUsername(params.username);
    }

    public static final class Params {

        private final String username;

        Params(String username) {
            this.username = username;
        }

        public static Params buildParams(String username) {
            return new Params(username);
        }
    }
}
