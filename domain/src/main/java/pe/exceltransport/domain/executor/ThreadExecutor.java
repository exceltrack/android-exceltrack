package pe.exceltransport.domain.executor;


import java.util.concurrent.Executor;

public interface ThreadExecutor extends Executor {}
