package pe.exceltransport.domain;

public class Driver {

    private long id;

    private User user;

    private Vehicle tractor;

    private Vehicle trailer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vehicle getTractor() {
        return tractor;
    }

    public void setTractor(Vehicle tractor) {
        this.tractor = tractor;
    }

    public Vehicle getTrailer() {
        return trailer;
    }

    public void setTrailer(Vehicle trailer) {
        this.trailer = trailer;
    }

}
