package pe.exceltransport.domain.repository;

import io.reactivex.Observable;
import pe.exceltransport.domain.Event;
import pe.exceltransport.domain.Tracking;

public interface TrackingRepository {

    Observable<Tracking> getTracking(String token, long tripId);

    Observable<Tracking> addEvent(String token, long trackingId, Event event);

}
