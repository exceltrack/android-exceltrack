package pe.exceltransport.domain.repository;

import io.reactivex.Observable;
import pe.exceltransport.domain.Driver;
import pe.exceltransport.domain.Session;

public interface UserRepository {

    Observable<Session> signIn(final String email, final String password);

    Observable<Driver> getDriver(String token, long driverId);

    Observable<Void> saveSession(Session session);

    Observable<Void> saveUsername(final String email);

    Observable<String> getUsernameSaved();

    Observable<Session> getSessionSaved();

    Observable<Void> deleteSessionSaved();

}
