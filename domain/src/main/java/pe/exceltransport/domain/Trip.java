package pe.exceltransport.domain;

import java.io.Serializable;

public class Trip implements Serializable {

    private long id;

    private String referenceDocument;

    private String startDate;

    private String contactName;

    private String contactPhone;

    private Load load;

    private double initialFuelTank;

    private double finalFuelTank;

    private Location start;

    private Location finish;

    private Status status;

    private Customer customer;

    private Tracking tracking;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public Load getLoad() {
        return load;
    }

    public void setLoad(Load load) {
        this.load = load;
    }

    public double getInitialFuelTank() {
        return initialFuelTank;
    }

    public void setInitialFuelTank(double initialFuelTank) {
        this.initialFuelTank = initialFuelTank;
    }

    public double getFinalFuelTank() {
        return finalFuelTank;
    }

    public void setFinalFuelTank(double finalFuelTank) {
        this.finalFuelTank = finalFuelTank;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getFinish() {
        return finish;
    }

    public void setFinish(Location finish) {
        this.finish = finish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContectPhone() {
        return contactPhone;
    }

    public void setContectPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public enum Status implements Serializable {
        PENDING,
        ACTIVE,
        COMPLETED
    }

    public enum Load implements Serializable {
        REFRIGERATED_CONTAINER,
        DRY_LOOSE,
        DRY_CONTAINER_20,
        DRY_CONTAINER_40,
    }
}
