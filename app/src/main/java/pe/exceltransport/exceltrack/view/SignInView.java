package pe.exceltransport.exceltrack.view;


public interface SignInView extends LoadDataView {

    String getUsername();

    String getPassword();

    void setUsername(String email);

    void goToMain();

}
