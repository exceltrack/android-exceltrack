package pe.exceltransport.exceltrack.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.exceltransport.domain.Trip;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.view.util.DateUtil;

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ItemHolder> {

    private List<Trip> list;
    private OnItemClickListener listener;


    @Inject
    public TripAdapter() {
        this.list = new ArrayList<>();
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip_list, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.bind(list.get(holder.getLayoutPosition()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void bindList(List<Trip> list) {
        if (list != null) {
            this.list = list;
            notifyDataSetChanged();
        }
    }

    public class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_customer_name)
        TextView tvCustomerName;

        @BindView(R.id.tv_start)
        TextView tvStart;

        @BindView(R.id.tv_finish)
        TextView tvFinish;

        @BindView(R.id.tv_start_date)
        TextView tvStartDate;

        @BindView(R.id.iv_load)
        ImageView ivLoad;

        ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(Trip trip) {
            tvCustomerName.setText(trip.getCustomer().getCompany().getTradeName());
            tvStart.setText(trip.getStart().getAddress());
            tvFinish.setText(trip.getFinish().getAddress());
            tvStartDate.setText(trip.getStartDate() != null ? DateUtil.stringToDateFormattedWithoutTimeZone(trip.getStartDate(), DateUtil.DEFAULT_FORMAT): "");
            ivLoad.setImageResource(getLoadImage(trip.getLoad()));
        }

        private int getLoadImage(Trip.Load load){
            if(load.equals(Trip.Load.REFRIGERATED_CONTAINER)){
                return R.drawable.img_refrigerated_container;
            }else if(load.equals(Trip.Load.DRY_LOOSE)){
                return R.drawable.img_loose_load;
            }else if(load.equals(Trip.Load.DRY_CONTAINER_20)){
                return R.drawable.img_container_20;
            }else {
                return R.drawable.img_container_40;
            }
        }

        @OnClick(R.id.v_item)
        public void onClick() {
            if (listener != null) {
                listener.onItemClick(list.get(getLayoutPosition()));
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Trip trip);
    }
}
