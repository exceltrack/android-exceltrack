package pe.exceltransport.exceltrack.view;

import java.util.List;

import pe.exceltransport.domain.Trip;

public interface CompletedTripListView extends LoadDataView{

    void renderTrips(List<Trip> trips);

}
