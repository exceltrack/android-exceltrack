package pe.exceltransport.exceltrack.view;


import pe.exceltransport.domain.Driver;

public interface MoreView {

    void goToSignIn();

    void displayProfile(Driver driver);

}
