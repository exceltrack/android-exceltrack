package pe.exceltransport.exceltrack.view.dialog;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.view.util.Extra;

public class DefaultDialog extends BaseDialog {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    private OnCLickListener listener;

    public static DefaultDialog newInstance(String title, String description) {
        DefaultDialog dialog = new DefaultDialog();
        Bundle arg = new Bundle();
        arg.putString(Extra.TITLE.getValue(), title);
        arg.putString(Extra.DESCRIPTION.getValue(), description);
        dialog.setArguments(arg);
        return dialog;
    }

    public DefaultDialog() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null){
            window.requestFeature(Window.FEATURE_NO_TITLE);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        if (getDialog() != null){
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);
        }
        View view = inflater.inflate(R.layout.dialog_default, container, false);
        injectView(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            tvDescription.setText(arguments.getString(Extra.DESCRIPTION.getValue()));
            tvTitle.setText(arguments.getString(Extra.TITLE.getValue()));
        }
    }

    public void setListener(OnCLickListener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.btn_got_it)
    public void onBtnGotIt() {
        dismiss();
        if (listener != null) {
            listener.onClick();
        }
    }

    public interface OnCLickListener {
        void onClick();
    }
}
