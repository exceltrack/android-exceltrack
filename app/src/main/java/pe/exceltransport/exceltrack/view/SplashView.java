package pe.exceltransport.exceltrack.view;

public interface SplashView extends  LoadDataView{

    void goToSignIn();

    void goToMain();
}
