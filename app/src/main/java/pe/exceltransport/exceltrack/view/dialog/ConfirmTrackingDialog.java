package pe.exceltransport.exceltrack.view.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.OnClick;
import pe.exceltransport.exceltrack.R;

public class ConfirmTrackingDialog extends BaseDialog {

    private OnCLickListener listener;

    public static ConfirmTrackingDialog newInstance() {
        return new ConfirmTrackingDialog();
    }

    public ConfirmTrackingDialog() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null){
            window.requestFeature(Window.FEATURE_NO_TITLE);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        if (getDialog() != null){
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);
        }
        View view = inflater.inflate(R.layout.dialog_confirm_tracking, container, false);
        injectView(this, view);
        return view;
    }

    public void setListener(OnCLickListener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.btn_got_it)
    public void onBtnGotIt() {
        dismiss();
        if (listener != null) {
            listener.onConfirm();
        }
    }

    @OnClick(R.id.btn_cancel)
    public void onBtnCancel() {
        dismiss();
        if (listener != null) {
            listener.onCancel();
        }
    }

    public interface OnCLickListener {
        void onConfirm();
        void onCancel();
    }
}
