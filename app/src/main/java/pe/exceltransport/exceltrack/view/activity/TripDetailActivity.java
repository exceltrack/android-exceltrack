package pe.exceltransport.exceltrack.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.googlemaproute.DrawRoute;
import com.ebanx.swipebtn.OnActiveListener;
import com.ebanx.swipebtn.SwipeButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import pe.exceltransport.domain.Event;
import pe.exceltransport.domain.Location;
import pe.exceltransport.domain.Tracking;
import pe.exceltransport.domain.Trip;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.presenter.TripDetailPresenter;
import pe.exceltransport.exceltrack.view.TripDetailView;
import pe.exceltransport.exceltrack.view.adapter.EventTimeLineAdapter;
import pe.exceltransport.exceltrack.view.dialog.ConfirmTrackingDialog;
import pe.exceltransport.exceltrack.view.dialog.DefaultDialog;
import pe.exceltransport.exceltrack.view.util.DateUtil;
import pe.exceltransport.exceltrack.view.util.Extra;
import pe.exceltransport.exceltrack.view.util.PermissionUtil;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;


public class TripDetailActivity extends BaseActivity implements TripDetailView, OnActiveListener, EventTimeLineAdapter.OnItemClickListener, ConfirmTrackingDialog.OnCLickListener, DefaultDialog.OnCLickListener, DrawRoute.onDrawRoute {

    @BindView(R.id.map_loading)
    View vMapLoading;

    @BindView(R.id.event_loading)
    View vEventLoading;

    @BindView(R.id.button_loading)
    View vButtonLoading;

    @BindView(R.id.v_bottom_sheet)
    View vBottomSheet;

    @BindView(R.id.v_incident_container)
    View vIncidentContainer;

    @BindView(R.id.tv_completed)
    View vCompleted;

    @BindView(R.id.tv_customer_name)
    TextView tvCustomerName;

    @BindView(R.id.tv_start)
    TextView tvStart;

    @BindView(R.id.tv_finish)
    TextView tvFinish;

    @BindView(R.id.tv_document)
    TextView tvDocument;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;

    @BindView(R.id.tv_contact)
    TextView tv_contact;

    @BindView(R.id.iv_load)
    ImageView ivLoad;

    @BindView(R.id.rv_tracking)
    RecyclerView rvTracking;

    @BindView(R.id.swipe_button)
    SwipeButton swipeButton;

    @BindView(R.id.fab_events)
    FloatingActionButton fabEvents;

    @NotEmpty(messageResId = R.string.text_invalid_field)
    @BindView(R.id.til_incident_detail)
    TextInputLayout tilIncidentDetail;

    @Inject
    TripDetailPresenter presenter;

    @Inject
    EventTimeLineAdapter trackingAdapter;

    private Trip trip;

    private GoogleMap googleMap;

    private BottomSheetBehavior bottomSheetBehavior;

    public static Intent getCallingIntent(BaseActivity activity, Trip trip) {
        Intent intent = new Intent(activity, TripDetailActivity.class);
        intent.putExtra(Extra.TRIP.getValue(), trip);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_detail);
        injectView(this);
        presenter.setView(this);
        getExtras();
        requiresPermission();
    }

    @Override
    protected void initUI() {
        presenter.setCompleted(trip.getStatus().equals(Trip.Status.COMPLETED));
        presenter.mapListeners();
        presenter.start();
        setupBottomSheetBehavior();
        tvCustomerName.setText(trip.getCustomer().getCompany().getTradeName());
        tvStart.setText(trip.getStart().getAddress());
        tvFinish.setText(trip.getFinish().getAddress());
        ivLoad.setImageResource(getLoadImage(trip.getLoad()));
        tvDocument.setText(trip.getReferenceDocument());
        tvStartDate.setText(trip.getStartDate() != null ? DateUtil.stringToDateFormattedWithoutTimeZone(trip.getStartDate(), DateUtil.DEFAULT_FORMAT) : "");
        tv_contact.setText(String.format("%s - %s", trip.getContactName() != null ? trip.getContactName() : "", trip.getContectPhone() != null ? trip.getContectPhone() : ""));
        setupRecyclerView();
    }

    @OnClick(R.id.ib_location)
    public void onIbLocation() {
        moveCamera(true);
    }

    @OnClick(R.id.tv_contact)
    public void onTvContact() {
        if (isActionEnable()) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", trip.getContectPhone(), null));
            startActivity(intent);
        }
    }

    @OnClick(R.id.fab_events)
    public void onFabEvents() {
        if (trip.getStatus() != Trip.Status.COMPLETED && bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            vIncidentContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_cancel)
    public void onBtnCancel() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            hideKeyboard();
            vIncidentContainer.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_send)
    public void onBtnSend() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            hideKeyboard();
            vIncidentContainer.setVisibility(View.GONE);
            presenter.addTrackingEvent(Event.Type.INCIDENCE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void showError(String message) {
        navigator.showErrorDialog(getString(R.string.text_error), message, null);
    }

    @Override
    public void goToSignIn() {
        navigator.navigateToSignInActivity();
    }

    @Override
    public long getTripId() {
        return trip.getId();
    }

    @Override
    public long getTrackingId() {
        return trip.getTracking().getTrackingId();
    }

    @Override
    public Location getCurrentLocation() {
        Location currentLocation = new Location();
        currentLocation.setLatitude(presenter.getCurrentLocation().getLatitude());
        currentLocation.setLongitude(presenter.getCurrentLocation().getLongitude());
        return currentLocation;
    }

    @Override
    public String getDetail() {
        return tilIncidentDetail.getEditText() != null ? tilIncidentDetail.getEditText().getText().toString().trim() : "";
    }

    @Override
    public SupportMapFragment getSupportMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setRotateGesturesEnabled(false);
        addMarkersToMap();
        moveCamera(false);
    }

    @Override
    public void showMapLoading() {
        vMapLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMapLoading() {
        vMapLoading.setVisibility(View.GONE);
    }

    @Override
    public void showTrackingLoading() {
        swipeButton.setVisibility(View.INVISIBLE);
        vButtonLoading.setVisibility(View.VISIBLE);
        vEventLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTrackingLoading() {
        if (!trip.getStatus().equals(Trip.Status.COMPLETED)) {
            swipeButton.setVisibility(View.VISIBLE);
        }
        vEventLoading.setVisibility(View.GONE);
        vButtonLoading.setVisibility(View.GONE);
    }

    @Override
    public void renderTracking(Tracking tracking) {
        trip.setTracking(tracking);
        trackingAdapter.bindList(tracking.getEvents());
        addEventsToMap(tracking.getEvents());
        setupSwipeButton();
    }

    @Override
    public void showTracking() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            swipeButton.toggleState();
        }
        if (tilIncidentDetail.getEditText() != null) {
            tilIncidentDetail.getEditText().setText("");
        }

    }

    @Override
    public void tripCompleted() {
        trip.setStatus(Trip.Status.COMPLETED);
        presenter.updateSessionTrip(null);
        navigator.showErrorDialog("Felicitaciones", "Se ha completado el viaje. Buen día", this);
    }

    @Override
    public void updateTrip(Tracking tracking) {
        trip.setTracking(tracking);
        trip.setStatus(Trip.Status.ACTIVE);
        presenter.updateSessionTrip(trip);
    }

    @Override
    public void renderCompletedTracking(Tracking tracking) {
        trip.setTracking(tracking);
        trackingAdapter.bindList(tracking.getEvents());
        addEventsToMap(tracking.getEvents());
        vCompleted.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMarkerClick(Location location) {
        if (location != null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                    location.getLongitude()), 15));
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    @Override
    public void onActive() {
        navigator.showConfirmTrackingDialog(this);
    }

    @AfterPermissionGranted(PermissionUtil.LOCATION)
    private void requiresPermission() {
        String[] perms = {PermissionUtil.Permission.LOCATION.getPerm()};
        if (EasyPermissions.hasPermissions(this, perms)) {
            turnGPSOn();
            initUI();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(
                    new PermissionRequest.Builder(this, PermissionUtil.Permission.LOCATION.getCode(), perms)
                            .setRationale(R.string.location_rationale)
                            .setPositiveButtonText(R.string.rationale_ask_ok)
                            .build());
        }
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            super.onBackPressed();
        }
        if (vIncidentContainer.getVisibility() == View.VISIBLE) {
            hideKeyboard();
            vIncidentContainer.setVisibility(View.GONE);
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    private void getExtras() {
        trip = Trip.class.cast(getIntent().getSerializableExtra(Extra.TRIP.getValue()));
    }

    private int getLoadImage(Trip.Load load) {
        if (load.equals(Trip.Load.REFRIGERATED_CONTAINER)) {
            return R.drawable.img_refrigerated_container;
        } else if (load.equals(Trip.Load.DRY_LOOSE)) {
            return R.drawable.img_loose_load;
        } else if (load.equals(Trip.Load.DRY_CONTAINER_20)) {
            return R.drawable.img_container_20;
        } else {
            return R.drawable.img_container_40;
        }
    }

    private void addMarkersToMap() {

        LatLng origin = new LatLng(trip.getStart().getLatitude(), trip.getStart().getLongitude());
        LatLng destination = new LatLng(trip.getFinish().getLatitude(), trip.getFinish().getLongitude());

        DrawRoute.getInstance(this, this).setFromLatLong(trip.getStart().getLatitude(), trip.getStart().getLongitude())
                .setToLatLong(trip.getFinish().getLatitude(), trip.getFinish().getLongitude())
                .setGmapAndKey(getString(R.string.GOOGLE_GEO_API_KEY), googleMap)
                .setLoader(false)
                .run();

        addMarkerToMap(googleMap,
                origin,
                getString(R.string.text_start),
                trip.getStart().getAddress(),
                BitmapDescriptorFactory.fromResource(R.drawable.marker_flag));

        addMarkerToMap(googleMap,
                destination,
                getString(R.string.text_finish),
                trip.getFinish().getAddress(),
                BitmapDescriptorFactory.fromResource(R.drawable.marker_beenhere));

    }

    private void addEventsToMap(List<Event> events) {
        for (Event event : events) {
            if (event.getLocation() != null) {
                addMarkerToMap(googleMap,
                        new LatLng(event.getLocation().getLatitude(), event.getLocation().getLongitude()),
                        "",
                        event.getDetail(),
                        BitmapDescriptorFactory.fromResource(R.drawable.marker_place));
            }

        }
    }

    private void addMarkerToMap(GoogleMap googleMap, LatLng position, String title, String snippet, BitmapDescriptor icon) {
        googleMap.addMarker(new MarkerOptions()
                .icon(icon)
                .position(position)
                .title(title)
                .snippet(snippet));
    }

    private void moveCamera(boolean animate) {
        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(new LatLng(trip.getStart().getLatitude(), trip.getStart().getLongitude()))
                .include(new LatLng(trip.getFinish().getLatitude(), trip.getFinish().getLongitude()))
                .build();
        if (animate) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
        } else {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
        }
    }

    private void setupBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(vBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (!trip.getStatus().equals(Trip.Status.COMPLETED)) {
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                        fabEvents.animate().scaleX(1).scaleY(1).setDuration(300).start();
                    } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                        fabEvents.animate().scaleX(0).scaleY(0).setDuration(300).start();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    //default implementation
                }
            });
        }
    }

    private void setupRecyclerView() {
        trackingAdapter.setListener(this);
        rvTracking.setLayoutManager(new LinearLayoutManager(this));
        rvTracking.setHasFixedSize(true);
        rvTracking.setAdapter(trackingAdapter);
    }

    private void setupSwipeButton() {
        swipeButton.setVisibility(View.VISIBLE);
        swipeButton.setOnActiveListener(this);
        swipeButton.setText(getNextTrackName(trip.getTracking().getStatus(), trip.getLoad()));
    }

    private String getNextTrackName(Tracking.Status status, Trip.Load load) {
        switch (status) {
            case ASSIGNED:
                return getString(R.string.text_accept);
            case STARTED:
                return getString(R.string.text_go_to_starting);
            case CHECK_IN:
                return getString(R.string.text_starting_load);
            case LOAD:
                return getString(R.string.text_starting_shipment);
            case DEPARTURE:
                return getString(R.string.text_starting_arrival);
            case ARRIVAL:
                return getString(R.string.text_starting_unloading);
            case UNLOADING:
                return load.equals(Trip.Load.DRY_LOOSE) ?
                        getString(R.string.text_finished) :
                        getString(R.string.text_starting_return);
            default:
                return getString(R.string.text_finished);
        }
    }

    private void turnGPSOn() {
        if (!isGpsEnabled()) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    @Override
    public void onConfirm() {
        presenter.addTrackingEvent(Event.Type.TRACKING);
    }

    @Override
    public void onCancel() {
        swipeButton.toggleState();

    }

    @Override
    public void onClick() {
        finish();
    }

    @Override
    public void afterDraw(String result) {
        Log.d("response", "" + result);
    }
}
