package pe.exceltransport.exceltrack.view.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {

    private static final Locale locale = new Locale("es", "pe");

    public static final String DEFAULT_FORMAT ="dd-MM-yyyy h:mm a";

    public static String milliSecondsToDateFormatted(long milliSeconds, String dateFormat){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return new SimpleDateFormat(dateFormat,locale).format(calendar.getTime());
    }

    private static Date stringToDate(String stringDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
        Date date;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

    public static String stringToDateFormatted(String stringDate, String dateFormat){
        SimpleDateFormat timeFormat = new SimpleDateFormat(dateFormat,Locale.getDefault());
        timeFormat.setTimeZone(TimeZone.getTimeZone("GMT-10"));
        Date date = stringToDate(stringDate);
        return date != null ? timeFormat.format(date) : "";

    }

    public static String stringToDateFormattedWithoutTimeZone(String stringDate, String dateFormat){
        SimpleDateFormat timeFormat = new SimpleDateFormat(dateFormat,Locale.getDefault());
        Date date = stringToDate(stringDate);
        return date != null ? timeFormat.format(date) : "";

    }

}
