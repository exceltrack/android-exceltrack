package pe.exceltransport.exceltrack.view.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import dagger.android.support.AndroidSupportInjection;
import jp.wasabeef.blurry.Blurry;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.presenter.SignInPresenter;
import pe.exceltransport.exceltrack.view.SignInView;
import pe.exceltransport.exceltrack.view.activity.SignInActivity;
import pe.exceltransport.exceltrack.view.util.TextInputLayoutAdapter;

public class SignInFragment extends BaseFragment implements SignInView, Validator.ValidationListener, Validator.ViewValidatedAction {

    @Pattern(regex = "[a-zA-Z0-9]+", messageResId = R.string.text_invalid_field)
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;

    @Password(min = 8, messageResId = R.string.text_invalid_field)
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @Inject
    SignInPresenter presenter;

    @Inject
    Validator validator;

    private SignInActivity activity;

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectView(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (SignInActivity) getActivity();
        presenter.setView(this);
        setupValidator();
      //  initUI();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    protected void initUI() {
        presenter.getEmailSaved();
    }

    @Override
    public String getUsername() {
        return tilUsername.getEditText() != null ? tilUsername.getEditText().getText().toString().trim() : "";
    }

    @Override
    public String getPassword() {
        return tilPassword.getEditText() != null ? tilPassword.getEditText().getText().toString().trim() : "";
    }

    @Override
    public void setUsername(String email) {
        if (tilUsername.getEditText() != null) tilUsername.getEditText().setText(email);
    }

    @Override
    public void goToMain() {
        activity.getNavigator().navigateToMainActivity();
    }


    @OnClick(R.id.btn_sign_in)
    public void onBtnSignIn() {
        activity.hideKeyboard();
        validator.validate();
    }

    @OnEditorAction(R.id.et_password)
    protected boolean onEtPasswordActionDone(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onBtnSignIn();
            return true;
        }
        return false;
    }

    @OnTextChanged(value = R.id.et_username, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onEtEmailTextChanged() {
        tilUsername.setError("");
        tilUsername.setErrorEnabled(false);
    }

    @OnTextChanged(value = R.id.et_password, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onEtPasswordTextChanged() {
        tilPassword.setError("");
        tilPassword.setErrorEnabled(false);
    }

    @Override
    public void showLoading() {
        activity.showLoading();
    }

    @Override
    public void hideLoading() {
        activity.hideLoading();
    }

    @Override
    public void showError(String message) {
        activity.getNavigator().showErrorDialog(getString(R.string.text_error), message, null);
    }

    @Override
    public void goToSignIn() {
        //default implementation
    }

    @Override
    public void onValidationSucceeded() {
        presenter.signIn();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        errors.get(0).getView().requestFocus();
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(message);
                ((TextInputLayout) view).setErrorEnabled(true);
            }
        }
    }

    @Override
    public void onAllRulesPassed(View view) {
        if (view instanceof TextInputLayout) {
            ((TextInputLayout) view).setError("");
            ((TextInputLayout) view).setErrorEnabled(false);
        }
    }

    private void setupValidator() {
        validator.setValidationListener(this);
        validator.registerAdapter(TextInputLayout.class, new TextInputLayoutAdapter());
        validator.setViewValidatedAction(this);
    }

    public static Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(c);
        return bitmap;
    }

}
