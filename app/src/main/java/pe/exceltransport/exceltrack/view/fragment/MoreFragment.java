package pe.exceltransport.exceltrack.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import pe.exceltransport.domain.Driver;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.presenter.MorePresenter;
import pe.exceltransport.exceltrack.view.MoreView;
import pe.exceltransport.exceltrack.view.activity.MainActivity;

public class MoreFragment extends BaseFragment implements MoreView {

    @BindView(R.id.tv_full_name)
    TextView tvFullName;

    @BindView(R.id.tv_tractor)
    TextView tvTractor;

    @BindView(R.id.tv_trailer)
    TextView tvTrailer;

    @Inject
    MorePresenter presenter;

    private MainActivity activity;

    public static MoreFragment newInstance(){
        return new MoreFragment();
    }

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectView(this,view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MainActivity) getActivity();
        presenter.setView(this);
        initUI();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    protected void initUI() {
        setupToolbar();
        presenter.getProfile();
    }

    @OnClick(R.id.v_sign_out)
    public void onSignOut(){
        presenter.signOut();
    }

    @Override
    public void goToSignIn() {
        activity.getNavigator().navigateToSignInActivity();
    }

    @Override
    public void displayProfile(Driver driver) {
        tvFullName.setText(String.format("%s%n%s", driver.getUser().getName(), driver.getUser().getLastName()));
        if(driver.getTractor()!=null){
            tvTractor.setText(String.format("%s %s %s", driver.getTractor().getPlate(), driver.getTractor().getBrand(), driver.getTractor().getModel()));
        }
        if(driver.getTrailer()!=null){
            tvTrailer.setText(String.format("%s %s %s", driver.getTrailer().getPlate(), driver.getTrailer().getBrand(), driver.getTrailer().getModel()));
        }
    }

    private void setupToolbar(){
        activity.setToolbarTitle("Mi perfil");
    }
}
