package pe.exceltransport.exceltrack.view;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import pe.exceltransport.domain.Location;
import pe.exceltransport.domain.Tracking;

public interface TripDetailView extends LoadDataView {

    long getTripId();

    long getTrackingId();

    Location getCurrentLocation();

    String getDetail();

    SupportMapFragment getSupportMapFragment();

    void onMapReady(GoogleMap googleMap);

    void showMapLoading();

    void hideMapLoading();

    void showTrackingLoading();

    void hideTrackingLoading();

    void renderTracking(Tracking tracking);

    void showTracking();

    void tripCompleted();

    void updateTrip(Tracking tracking);

    void renderCompletedTracking(Tracking tracking);

}
