package pe.exceltransport.exceltrack.presenter;


import android.support.annotation.NonNull;

import javax.inject.Inject;

import pe.exceltransport.domain.Driver;
import pe.exceltransport.domain.Session;
import pe.exceltransport.domain.interactor.DefaultObserver;
import pe.exceltransport.domain.interactor.DeleteSessionSaved;
import pe.exceltransport.domain.interactor.GetDriver;
import pe.exceltransport.domain.interactor.GetSessionSaved;
import pe.exceltransport.exceltrack.view.MoreView;

public class MorePresenter implements Presenter<MoreView> {

    private MoreView view;

    private final DeleteSessionSaved deleteSessionSaved;

    private final GetSessionSaved getSessionSaved;

    private final GetDriver getDriver;

    @Inject
    public MorePresenter(DeleteSessionSaved deleteSessionSaved, GetSessionSaved getSessionSaved, GetDriver getDriver) {
        this.deleteSessionSaved = deleteSessionSaved;
        this.getSessionSaved = getSessionSaved;
        this.getDriver = getDriver;
    }

    @Override
    public void setView(@NonNull MoreView view) {
        this.view = view;
    }

    @Override
    public void resume() {
        //default implementation
    }

    @Override
    public void pause() {
        //default implementation
    }

    @Override
    public void destroy() {
        view = null;
        deleteSessionSaved.dispose();
        getSessionSaved.dispose();
        getDriver.dispose();
    }

    public void signOut(){
        deleteSessionSaved.execute(new SaveSessionObserver(),null);
    }

    public void getProfile() {
        getSessionSaved.execute(new GetSessionSavedObserver(), null);
    }

    private final class GetSessionSavedObserver extends DefaultObserver<Session> {

        @Override
        public void onNext(Session session) {
            super.onNext(session);
            view.displayProfile(session.getDriver());
            getDriver.execute(new GetDriverObserver(), GetDriver.Params.buildParams(session.getToken(),session.getDriver().getId()));
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            view.goToSignIn();
        }
    }

    private final class GetDriverObserver extends DefaultObserver<Driver> {

        @Override
        public void onNext(Driver driver) {
            super.onNext(driver);
            view.displayProfile(driver);
        }

    }

    private final class SaveSessionObserver extends DefaultObserver<Void> {

        @Override
        public void onComplete() {
            super.onComplete();
            view.goToSignIn();
        }
    }
}
