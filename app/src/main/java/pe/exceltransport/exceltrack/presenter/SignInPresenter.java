package pe.exceltransport.exceltrack.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import javax.inject.Inject;

import pe.exceltransport.data.exception.DefaultException;
import pe.exceltransport.domain.Session;
import pe.exceltransport.domain.interactor.DefaultObserver;
import pe.exceltransport.domain.interactor.GetUsernameSaved;
import pe.exceltransport.domain.interactor.SaveUsername;
import pe.exceltransport.domain.interactor.SaveSession;
import pe.exceltransport.domain.interactor.SignIn;
import pe.exceltransport.exceltrack.exception.ErrorMessageFactory;
import pe.exceltransport.exceltrack.view.SignInView;

public class SignInPresenter implements Presenter<SignInView> {

    private SignInView view;

    private final SignIn signIn;
    private final SaveUsername saveUsername;
    private final GetUsernameSaved getUsernameSaved;
    private final SaveSession saveSession;
    private final Context context;

    @Inject
    SignInPresenter(Context context, SignIn signIn, SaveUsername saveUsername, GetUsernameSaved getUsernameSaved, SaveSession saveSession) {
        this.context = context;
        this.signIn = signIn;
        this.saveSession = saveSession;
        this.saveUsername = saveUsername;
        this.getUsernameSaved = getUsernameSaved;
    }

    @Override
    public void setView(@NonNull SignInView view) {
        this.view = view;
    }

    @Override
    public void resume() {
        //default implementation
    }

    @Override
    public void pause() {
        //default implementation
    }

    @Override
    public void destroy() {
        view = null;
        signIn.dispose();
        saveSession.dispose();
        saveUsername.dispose();
        getUsernameSaved.dispose();
    }

    public void getEmailSaved() {
        getUsernameSaved.execute(new GetEmailSavedObserver(),null);
    }

    public void signIn() {
        signIn.execute(new SignInObserver(), SignIn.Params.buildParams(view.getUsername(), encode(view.getPassword())));
    }

    private final class GetEmailSavedObserver extends DefaultObserver<String> {

        @Override
        public void onNext(String email) {
            super.onNext(email);
            view.setUsername(email);
        }
    }

    private String encode(String text){
       return new String(Hex.encodeHex(DigestUtils.sha256(text)));
    }

    private final class SignInObserver extends DefaultObserver<Session> {

        private void saveSession(Session session) {
            saveSession.execute(new SaveSessionObserver(), SaveSession.Params.buildParams(session));
        }

        @Override
        protected void onStart() {
            super.onStart();
            view.showLoading();
        }

        @Override
        public void onComplete() {
            super.onComplete();
            view.hideLoading();
        }

        @Override
        public void onNext(Session session) {
            super.onNext(session);
            saveSession(session);
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            view.hideLoading();
            view.showError(ErrorMessageFactory.create(context, (DefaultException) exception));
        }
    }

    private final class SaveSessionObserver extends DefaultObserver<Void> {

        private void saveUsername() {
            saveUsername.execute(new SaveUsernameObserver(), SaveUsername.Params.buildParams(view.getUsername()));
        }

        @Override
        public void onComplete() {
            super.onComplete();
                saveUsername();
        }
    }

    private final class SaveUsernameObserver extends DefaultObserver<Void> {

        @Override
        public void onComplete() {
            super.onComplete();
            view.goToMain();
        }
    }


}
