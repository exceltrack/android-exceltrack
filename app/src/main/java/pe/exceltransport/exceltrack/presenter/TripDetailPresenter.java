package pe.exceltransport.exceltrack.presenter;


import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewTreeObserver;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import javax.inject.Inject;

import pe.exceltransport.data.exception.DefaultException;
import pe.exceltransport.domain.Event;
import pe.exceltransport.domain.Session;
import pe.exceltransport.domain.Tracking;
import pe.exceltransport.domain.Trip;
import pe.exceltransport.domain.interactor.AddEvent;
import pe.exceltransport.domain.interactor.DefaultObserver;
import pe.exceltransport.domain.interactor.GetSessionSaved;
import pe.exceltransport.domain.interactor.GetTracking;
import pe.exceltransport.domain.interactor.SaveSession;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.exception.ErrorMessageFactory;
import pe.exceltransport.exceltrack.internal.bus.EventBus;
import pe.exceltransport.exceltrack.internal.bus.RxBus;
import pe.exceltransport.exceltrack.view.TripDetailView;
import pe.exceltransport.exceltrack.view.util.LocationProvider;

public class TripDetailPresenter implements Presenter<TripDetailView>, LocationProvider.LocationListener {

    private TripDetailView view;

    private SupportMapFragment supportMapFragment;
    private View mapView;

    private boolean isViewReady;
    private boolean isMapReady;
    private GoogleMap googleMap;

    private Session currentSession;

    private final LocationProvider locationProvider;
    private final GetSessionSaved getSessionSaved;
    private final GetTracking getTracking;
    private final AddEvent addEvent;
    private final SaveSession saveSession;
    private final RxBus rxBus;
    private final Context context;

    private boolean isCompleted;

    @Inject
    TripDetailPresenter(Context context,
                        GetSessionSaved getSessionSaved,
                        GetTracking getTracking,
                        AddEvent addEvent,
                        LocationProvider locationProvider,
                        RxBus rxBus,
                        SaveSession saveSession) {
        this.context = context;
        this.getSessionSaved = getSessionSaved;
        this.getTracking = getTracking;
        this.addEvent = addEvent;
        this.saveSession = saveSession;
        this.locationProvider = locationProvider;
        this.rxBus = rxBus;
        this.isViewReady = false;
        this.isMapReady = false;
        this.googleMap = null;
    }

    @Override
    public void setView(@NonNull TripDetailView view) {
        this.view = view;
        this.supportMapFragment = view.getSupportMapFragment();
        this.mapView = supportMapFragment.getView();
    }

    @Override
    public void resume() {
        locationProvider.startLocationUpdates();
    }

    @Override
    public void pause() {
        //default implementation
    }

    @Override
    public void destroy() {
        view = null;
        isViewReady = false;
        isMapReady = false;
        googleMap = null;
        getTracking.dispose();
        addEvent.dispose();
    }

    public void mapListeners() {
        view.showMapLoading();
        locationProvider.setListener(this);
        locationProvider.connect();
        locationProvider.startLocationUpdates();
        if ((mapView.getWidth() != 0) && (mapView.getHeight() != 0)) {
            isViewReady = true;
        } else {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new GlobalLayoutObserver());
        }
        supportMapFragment.getMapAsync(new MapReadyObserver());
    }

    private void fireCallbackIfReady() {
        if (isViewReady && isMapReady) {
            view.hideMapLoading();
            view.onMapReady(googleMap);
        }
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public void start() {
        getSessionSaved.execute(new GetSessionSavedObserver(), null);
    }

    public void addTrackingEvent(Event.Type type) {
        if (isValidLocation()) {
            Event event = new Event();
            event.setType(type);
            if (type == Event.Type.INCIDENCE) {
                if (!view.getDetail().isEmpty()) {
                    event.setDetail(view.getDetail());
                } else {
                    view.showError("Se debe ingresar un detalle");
                    return;
                }
            }
            event.setLocation(view.getCurrentLocation());
            addEvent.execute(new AddEventObserver(), AddEvent.Params.buildParams(currentSession.getToken(), view.getTrackingId(), event));
        } else {
            view.showError(context.getString(R.string.text_gps_problem));
        }
    }

    private boolean isValidLocation() {
        return getCurrentLocation() != null;
    }

    public Location getCurrentLocation() {
        return locationProvider.getCurrentLocation();
    }

    @Override
    public void onNewLocation(Location location) {
        //default implementation
    }

    @Override
    public void onLocationNotAvailable() {
        //default implementation
    }

    public void updateSessionTrip(Trip trip) {
        currentSession.setTrip(trip);
        saveSession.execute(new SaveSessionObserver(), SaveSession.Params.buildParams(currentSession));
    }

    private final class GlobalLayoutObserver implements ViewTreeObserver.OnGlobalLayoutListener {

        @Override
        public void onGlobalLayout() {
            mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            isViewReady = true;
            fireCallbackIfReady();
        }
    }

    private final class MapReadyObserver implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            TripDetailPresenter.this.googleMap = googleMap;
            TripDetailPresenter.this.isMapReady = true;
            fireCallbackIfReady();
        }
    }

    private final class GetSessionSavedObserver extends DefaultObserver<Session> {

        @Override
        public void onNext(Session session) {
            super.onNext(session);
            currentSession = session;
            getTracking.execute(new GetTrackingObserver(), GetTracking.Params.buildParams(currentSession.getToken(), view.getTripId()));
        }

    }

    private class GetTrackingObserver extends DefaultObserver<Tracking> {

        @Override
        protected void onStart() {
            super.onStart();
            view.showTrackingLoading();
        }

        @Override
        public void onComplete() {
            super.onComplete();
            view.hideTrackingLoading();
        }

        @Override
        public void onNext(Tracking tracking) {
            super.onNext(tracking);
            if (isCompleted) {
                view.renderCompletedTracking(tracking);
            } else if (tracking.getStatus() == Tracking.Status.COMPLETED) {
                view.tripCompleted();
            } else if (tracking.getStatus() == Tracking.Status.ASSIGNED) {
                view.renderTracking(tracking);
            } else {
                view.renderTracking(tracking);
                view.updateTrip(tracking);
            }
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            view.hideLoading();
            view.hideTrackingLoading();
            view.showError(ErrorMessageFactory.create(context, (DefaultException) exception));
        }
    }

    private final class AddEventObserver extends GetTrackingObserver {

        @Override
        protected void onStart() {
            super.onStart();
            view.showLoading();
        }

        @Override
        public void onNext(Tracking tracking) {
            super.onNext(tracking);
            view.showTracking();
        }

    }

    private final class SaveSessionObserver extends DefaultObserver<Void> {

        @Override
        public void onComplete() {
            super.onComplete();
            sendBus();
            view.hideLoading();
        }

        private void sendBus() {
            rxBus.send(new EventBus.UpdateTripStatusEvent());
        }
    }

}
