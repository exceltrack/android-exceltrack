package pe.exceltransport.exceltrack.navigator;

import android.support.v4.app.FragmentTransaction;

import javax.inject.Inject;

import pe.exceltransport.domain.Trip;
import pe.exceltransport.exceltrack.R;
import pe.exceltransport.exceltrack.view.activity.BaseActivity;
import pe.exceltransport.exceltrack.view.activity.MainActivity;
import pe.exceltransport.exceltrack.view.activity.SignInActivity;
import pe.exceltransport.exceltrack.view.activity.TripDetailActivity;
import pe.exceltransport.exceltrack.view.dialog.ConfirmTrackingDialog;
import pe.exceltransport.exceltrack.view.dialog.DefaultDialog;
import pe.exceltransport.exceltrack.view.fragment.CompletedTripListFragment;
import pe.exceltransport.exceltrack.view.fragment.MoreFragment;
import pe.exceltransport.exceltrack.view.fragment.SignInFragment;
import pe.exceltransport.exceltrack.view.fragment.SplashFragment;
import pe.exceltransport.exceltrack.view.fragment.TripListFragment;

public class Navigator {

    private BaseActivity activity;

    @Inject
    public Navigator(BaseActivity activity) {
        this.activity = activity;
    }

    //navigate to activities
    public void navigateToSignInActivity() {
        activity.finish();
        activity.startActivity(SignInActivity.getCallingIntent(activity));
    }

    public void navigateToMainActivity() {
        activity.finish();
        activity.startActivity(MainActivity.getCallingIntent(activity));
    }

    public void navigateToTripDetailActivity(Trip trip) {
        activity.startActivity(TripDetailActivity.getCallingIntent(activity, trip));
    }

    //navigate to fragments
    public void navigateToSplashFragment() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, SplashFragment.newInstance(), SplashFragment.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    public void navigateToSignInFragment() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.add(R.id.fragment_container, SignInFragment.newInstance(), SignInFragment.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    public void navigateToTripListFragment() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, TripListFragment.newInstance(), TripListFragment.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    public void navigateToCompletedTripListFragment() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, CompletedTripListFragment.newInstance(), CompletedTripListFragment.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    public void navigateToMoreFragment() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, MoreFragment.newInstance(), MoreFragment.class.getSimpleName());
        fragmentTransaction(transaction);
    }


    public void showErrorDialog(String title, String message, DefaultDialog.OnCLickListener listener) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        DefaultDialog dialog = DefaultDialog.newInstance(title, message);
        dialog.setListener(listener);
        transaction.add(dialog, DefaultDialog.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    public void showConfirmTrackingDialog(ConfirmTrackingDialog.OnCLickListener listener) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        ConfirmTrackingDialog dialog = ConfirmTrackingDialog.newInstance();
        dialog.setListener(listener);
        transaction.add(dialog, ConfirmTrackingDialog.class.getSimpleName());
        fragmentTransaction(transaction);
    }

    private void fragmentTransaction(FragmentTransaction transaction) {
        if (activity.isStopped()) transaction.commitAllowingStateLoss();
        else transaction.commit();
    }

}
