package pe.exceltransport.exceltrack.internal.di.module;


import dagger.Module;
import dagger.Provides;
import pe.exceltransport.exceltrack.navigator.Navigator;
import pe.exceltransport.exceltrack.view.activity.SignInActivity;

@Module
public class SignInActivityModule {

    @Provides
    Navigator provideNavigator(SignInActivity activity) {
        return new Navigator(activity);
    }

}
