package pe.exceltransport.exceltrack.internal.di.module;

import com.mobsandgeeks.saripaar.Validator;

import dagger.Module;
import dagger.Provides;
import pe.exceltransport.domain.executor.PostExecutionThread;
import pe.exceltransport.domain.executor.ThreadExecutor;
import pe.exceltransport.domain.interactor.AddEvent;
import pe.exceltransport.domain.interactor.GetSessionSaved;
import pe.exceltransport.domain.interactor.GetTracking;
import pe.exceltransport.domain.interactor.SaveSession;
import pe.exceltransport.domain.repository.TrackingRepository;
import pe.exceltransport.domain.repository.UserRepository;
import pe.exceltransport.exceltrack.AndroidApplication;
import pe.exceltransport.exceltrack.internal.bus.RxBus;
import pe.exceltransport.exceltrack.navigator.Navigator;
import pe.exceltransport.exceltrack.view.activity.TripDetailActivity;

@Module
public class TripDetailActivityModule {

    @Provides
    Navigator provideNavigator(TripDetailActivity activity) {
        return new Navigator(activity);
    }

    @Provides
    Validator provideValidator(TripDetailActivity activity) {
        return new Validator(activity);
    }

    @Provides
    GetSessionSaved provideGetSessionSaved(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread){
        return new GetSessionSaved(repository,threadExecutor,postExecutionThread);
    }

    @Provides
    GetTracking provideGetTracking(TrackingRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GetTracking(repository,threadExecutor,postExecutionThread);
    }

    @Provides
    AddEvent provideAddEvent(TrackingRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new AddEvent(repository,threadExecutor,postExecutionThread);
    }

    @Provides
    SaveSession provideSaveSession(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new SaveSession(repository, threadExecutor, postExecutionThread);
    }

    @Provides
    RxBus provideRxBus(AndroidApplication application){
        return application.getBus();
    }

}