package pe.exceltransport.exceltrack.internal.di.module;

import dagger.Module;
import dagger.Provides;
import pe.exceltransport.data.repository.TrackingDataRepository;
import pe.exceltransport.data.repository.TripDataRepository;
import pe.exceltransport.data.repository.UserDataRepository;
import pe.exceltransport.domain.repository.TrackingRepository;
import pe.exceltransport.domain.repository.TripRepository;
import pe.exceltransport.domain.repository.UserRepository;

@Module
public class RepositoryModule {

    @Provides
    UserRepository provideUserRepository(UserDataRepository repository) {
        return repository;
    }

    @Provides
    TripRepository provideTripRepository(TripDataRepository repository) {
        return repository;
    }

    @Provides
    TrackingRepository provideTrackingRepository(TrackingDataRepository repository) {
        return repository;
    }
}
