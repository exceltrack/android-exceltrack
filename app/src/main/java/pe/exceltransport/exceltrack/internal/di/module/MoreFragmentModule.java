package pe.exceltransport.exceltrack.internal.di.module;

import dagger.Module;
import dagger.Provides;
import pe.exceltransport.domain.executor.PostExecutionThread;
import pe.exceltransport.domain.executor.ThreadExecutor;
import pe.exceltransport.domain.interactor.DeleteSessionSaved;
import pe.exceltransport.domain.interactor.GetDriver;
import pe.exceltransport.domain.interactor.GetSessionSaved;
import pe.exceltransport.domain.repository.UserRepository;

@Module
public class MoreFragmentModule {

    @Provides
    GetDriver provideGetDriver(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GetDriver(repository, threadExecutor, postExecutionThread);
    }

    @Provides
    GetSessionSaved provideGetSessionSaved(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread){
        return new GetSessionSaved(repository,threadExecutor,postExecutionThread);
    }

    @Provides
    DeleteSessionSaved provideSaveSession(UserRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new DeleteSessionSaved(repository, threadExecutor, postExecutionThread);
    }

}
